%{
  #include "PropertyList.h"
%}

%include stl.i     // We need this to use stl container
%include stdint.i  // to support uint8_t, uint32_t, etc.
%include std_string.i
%include std_vector.i

namespace std {
   %template(StringVector) vector<string>;
}
%ignore Euclid::PropertyList::value_acc;

%include "PropertyList.h"