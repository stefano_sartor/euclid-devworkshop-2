// Suppress swig complaints
#pragma SWIG nowarn=321  // swig warning on 'set', that is a built-in name in python

%{

#include "PropertyList.h"

#include "swig/PropertyListIterator.hpp"
%}

// Generic standard exceptions handling
%include "exception.i"

%exception {
  try {
    $action
  } catch (const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  }
}


%include stl.i // We need this to use stl container
%include stdint.i  // to support uint8_t, uint32_t, etc.
%include std_string.i
%include std_vector.i


namespace std {
   %template(StringVector) vector<string>;
}

namespace Euclid {
 
class PropertyList {
public:

    PropertyList();

    template<typename T>
    void set(const std::string& key, const T& e, const std::string& comment="", bool inPlace=true);
    template<typename T>
    void add(const std::string& key, const T& e, const std::string& comment="");  

    template<typename T>
    T get(const std::string& key);    

    std::vector<std::string> keys();

    bool hasKey(const std::string& key, bool parent = true) const;

};

%define PropertyListAddType(Type)
%template(set) PropertyList::set<Type>;
%template(add) PropertyList::add<Type>;
%enddef

PropertyListAddType(bool)
PropertyListAddType(int)
PropertyListAddType(int64_t)
PropertyListAddType(double)


   
struct stop_iteration {};
 
%typemap(throws) stop_iteration %{
  (void)$1;
  SWIG_SetErrorObj(PyExc_StopIteration, SWIG_Py_Void());
  SWIG_fail;
  %}

class PropertyListIterator {
public:
  typedef PropertyList::iterator iterator;
  PropertyListIterator(iterator curr, iterator end);
  PyObject* next() throw(stop_iteration);
  PropertyListIterator&  __iter__();
};

 

%extend PropertyList {
  PyObject* get(const std::string& key, PyObject* defaultValue = nullptr) 
  {
    
    PyObject * result = nullptr;
    try {
      const std::type_info& t = self->typeOf(key);
      
      if (t == typeid(bool))               result =  PyBool_FromLong(self->get<bool>(key));
      if (t == typeid(char))               result =  PyInt_FromLong(self->get<char>(key));
      if (t == typeid(unsigned char))      result =  PyInt_FromLong(self->get<unsigned char>(key));
      if (t == typeid(short))              result =  PyInt_FromLong(self->get<short>(key));
      if (t == typeid(unsigned short))     result =  PyInt_FromLong(self->get<unsigned short>(key));
      if (t == typeid(int))                result =  PyInt_FromLong(self->get<int>(key));
      if (t == typeid(unsigned int))       result =  PyLong_FromUnsignedLong(self->get<unsigned int>(key));
      if (t == typeid(long))               result =  PyLong_FromLongLong(self->get<long>(key));
      if (t == typeid(unsigned long))      result =  PyLong_FromUnsignedLongLong(self->get<unsigned long>(key));
      if (t == typeid(long long))          result =  PyLong_FromLongLong(self->get<long long>(key));
      if (t == typeid(unsigned long long)) result =  PyLong_FromUnsignedLongLong(self->get<unsigned long long>(key));
      if (t == typeid(float))              result =  PyFloat_FromDouble(self->get<float>(key));    
      if (t == typeid(double))             result =  PyFloat_FromDouble(self->get<double>(key)); 
      if (t == typeid(std::string))
      {
        std::string val = self->get<std::string>(key);
        result =  PyString_FromStringAndSize(val.c_str(), val.size());
      }
    }
    catch (const std::out_of_range& e) {
      if (defaultValue)
        return defaultValue;
      else
        throw e;    
    }
    if (! result)
      throw std::runtime_error("Cannot convert key vlaue");
    return result;
  }


  Euclid::PropertyListIterator
  __iter__()
  {
    return Euclid::PropertyListIterator($self->begin(), $self->end());
  } 
  
  %pythoncode %{

  def items(self):
    l = []
    for i in self:
      l.append(i)
    return l
    
  %}
  
  
}

 
} // namespace Euclid

