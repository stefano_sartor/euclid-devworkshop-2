#ifndef PROPERTYLISTITERATOR_H
#define PROPERTYLISTITERATOR_H

#include <iostream>
#include "PropertyList.h"

namespace Euclid {

struct stop_iteration {};

class PropertyListIterator {

public:
  typedef PropertyList::iterator iterator;
    
  PropertyListIterator(iterator curr, iterator end)
    :curr_(curr), end_(end) 
  {
  }

  PyObject*
  next() throw(stop_iteration){
    if (curr_ != end_)
    {
      PyObject * result = PyTuple_New(3);
      const std::string& key = std::get<PropertyList::KEY>(*curr_);
      const std::string& comm = std::get<PropertyList::COM>(*curr_);

      PyTuple_SetItem(result,0,PyString_FromStringAndSize(key.c_str(), key.size()));
      PyTuple_SetItem(result,1,getPyValue());
      PyTuple_SetItem(result,2,PyString_FromStringAndSize(comm.c_str(), comm.size()));

      ++curr_;
      return result;
    }
    throw stop_iteration();
  }

  PropertyListIterator&
  __iter__()
  {
    return *this;
  }

  ~PropertyListIterator(){
    std::cout << "Del PropertyListIterator @" << (void*) this << std::endl;
  }


private:
    PyObject* getPyValue() {
      
      PyObject * result = nullptr;
      
      boost::any& item = std::get<PropertyList::VAL>(*curr_);
      const std::type_info& t = item.type();
      
      if (t == typeid(bool))               result =  PyBool_FromLong(boost::any_cast<bool>(item));
      if (t == typeid(char))               result =  PyInt_FromLong(boost::any_cast<char>(item));
      if (t == typeid(unsigned char))      result =  PyInt_FromLong(boost::any_cast<unsigned char>(item));
      if (t == typeid(short))              result =  PyInt_FromLong(boost::any_cast<short>(item));
      if (t == typeid(unsigned short))     result =  PyInt_FromLong(boost::any_cast<unsigned short>(item));
      if (t == typeid(int))                result =  PyInt_FromLong(boost::any_cast<int>(item));
      if (t == typeid(unsigned int))       result =  PyLong_FromUnsignedLong(boost::any_cast<unsigned int>(item));
      if (t == typeid(long))               result =  PyLong_FromLongLong(boost::any_cast<long>(item));
      if (t == typeid(unsigned long))      result =  PyLong_FromUnsignedLongLong(boost::any_cast<unsigned long>(item));
      if (t == typeid(long long))          result =  PyLong_FromLongLong(boost::any_cast<long long>(item));
      if (t == typeid(unsigned long long)) result =  PyLong_FromUnsignedLongLong(boost::any_cast<unsigned long long>(item));
      if (t == typeid(float))              result =  PyFloat_FromDouble(boost::any_cast<float>(item));    
      if (t == typeid(double))             result =  PyFloat_FromDouble(boost::any_cast<double>(item)); 
      if (t == typeid(std::string)){
	std::string val = boost::any_cast<std::string>(item);
	result =  PyString_FromStringAndSize(val.c_str(), val.size());
      }

      if (! result)
	throw std::runtime_error("Cannot convert value");
      return result;
    }
  
  iterator curr_;
  iterator end_;
  
};
  
}

#endif
