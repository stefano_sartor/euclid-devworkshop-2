// Suppress swig complaints
#pragma SWIG nowarn=321  // swig warning on 'set', that is a built-in name in python

%{

#include "PropertyList.h"

#include "swig/PropertyListIterator.hpp"
%}

// Generic standard exceptions handling
%include "exception.i"

%include stl.i // We need this to use stl container
%include stdint.i  // to support uint8_t, uint32_t, etc.
%include std_string.i
%include std_vector.i


namespace std {
   %template(StringVector) vector<string>;
}

namespace Euclid {

class PropertyList {
public:

    PropertyList();

    template<typename T>
    void set(const std::string& key, const T& e, const std::string& comment="", bool inPlace=true);
    template<typename T>
    void add(const std::string& key, const T& e, const std::string& comment=""); 
    
    bool getAsBool(const std::string& key);

    int getAsInt(std::string const& key);

    int64_t getAsInt64(std::string const& key);

    double getAsDouble(std::string const& key);

    std::vector<std::string> keys();

    bool hasKey(const std::string& key, bool parent = true) const;

};

%define PropertyListAddType(Type)
%template(set) PropertyList::set<Type>;
%template(add) PropertyList::add<Type>;
%enddef

PropertyListAddType(bool)
PropertyListAddType(int)
PropertyListAddType(int64_t)
PropertyListAddType(double)


struct stop_iteration {};
 
%typemap(throws) stop_iteration %{
  (void)$1;
  SWIG_SetErrorObj(PyExc_StopIteration, SWIG_Py_Void());
  SWIG_fail;
  %}

class PropertyListIterator {
public:
  typedef PropertyList::iterator iterator;
  PropertyListIterator(iterator curr, iterator end);
  PyObject* next() throw(stop_iteration);
  PropertyListIterator& __iter__();
};


%extend PropertyList {

  Euclid::PropertyListIterator
  __iter__()
  {
    return Euclid::PropertyListIterator($self->begin(), $self->end());
  }

  %pythoncode %{ 

  def items(self):
    l = []
    for i in self:
      l.append(i)
    return l
    
  %}
  
  
}



} // namespace Euclid

