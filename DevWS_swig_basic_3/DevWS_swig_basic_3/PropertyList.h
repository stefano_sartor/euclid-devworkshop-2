#ifndef EUCLID_IMG_UTILS_PROPERTYLIST_H
#define EUCLID_IMG_UTILS_PROPERTYLIST_H

/*
 * Copyright (C) 2012-2020 Euclid Science Ground Segment
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <memory>
#include <string>
#include <list>
#include <vector>
#include <exception>
#include <boost/any.hpp>


namespace Euclid{

//class PropertyListIterator;


class PropertyList {
public:
    using size_type = int;
    using value_type = std::tuple<const std::string,boost::any,std::string>; // (key, value, comment)
    enum value_acc {KEY=0, VAL=1,COM=2};
    using reference = value_type&;
    using const_reference = const value_type&;
    using iterator = std::list<value_type>::iterator;
    using const_iterator = std::list<value_type>::const_iterator;

    PropertyList();

    template<typename T>
    void set(const std::string& key, const T& e, const std::string& comment="", bool inPlace=true);
    template<typename T>
    void add(const std::string& key, const T& e, const std::string& comment="");


    void set(const std::string& key, const char* e, const std::string& comment="", bool inPlace=true);
    void add(const std::string& key, const char* e, const std::string& comment="");


    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the template type argument is not compatible
    template<typename T>
    T get(const std::string& key);

    // throws boost::bad_any_cast if the template type argument is not compatible and the key is present
    template<typename T>
    T get(const std::string& key, const T& defaultValue);

    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the underline type is not a bool
    bool getAsBool(const std::string& key);

    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the underline type is not lossless convertible to int
    int getAsInt(std::string const& key);

    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the underline type is not lossless convertible to int64_t
    int64_t getAsInt64(std::string const& key);

    // throws std::out_of_range if key is not present
    // throws boost::bad_any_cast if the underline type is not convertible to double
    double getAsDouble(std::string const& key);

    PropertyList makeChild();

    void setParent(const PropertyList& p);

    PropertyList getParent() const;

    bool hasParent()const;

    iterator begin();
    const_iterator begin()const;
    const_iterator cbegin()const;

    iterator end();
    const_iterator end()const;
    const_iterator cend()const;

    const std::type_info& typeOf(std::string const& name) const;

    PropertyList copy()const;

    void print();///DEBUG

    bool hasKey(const std::string& key, bool parent = true) const;

    std::vector<std::string> keys() const;
private:
    class Imp;

    PropertyList(std::shared_ptr<Imp>);
    std::shared_ptr<Imp> _impl;
};


class CastException : public std::bad_cast {
 public:
  CastException();
  CastException(const std::string& msg);
  virtual const char* what() const noexcept;
 private:
  std::string _msg;
};


}//namespace Euclid

#endif
